[![Coverage Status](https://coveralls.io/repos/gitlab/development-incolume/incolumepy.singleton_decorator/badge.svg?branch=master)](https://coveralls.io/gitlab/development-incolume/incolumepy.singleton_decorator?branch=master)
# incolumepy.singleton_decorator

Decorator singleton for methods, classes and others objects python

## Installing
```
$ pip install incolumepy.singleton-decorator
```

## Usage
For use import package singleton_decorator
```
from incolumepy.singleton_decorator import singleton
```

## Example
### Using functions
```
  1 from incolumepy.singleton_decorator import singleton
  2 @singleton
  3 def gennum():
  4     n = 0
  5     while True:
  6         yield n
  7         n += 1
  8 
  9 a = gennum()
 10 b = gennum()
 11 assert id(a) == id(b), "Not same object"
 12 print([next(a) for x in range(5)])
 13 print([next(b) for x in range(5)])

```

### Using classes
```
  1 from incolumepy.singleton_decorator import singleton
 14 (...)
 15 
 16 
 17 @singleton
 18 class GenNum:
 19     def __init__(self):
 20         self.n = 0
 21     def __iter__():
 22         return self
 23     def __next__(self):
 24         self.n += 1
 25         return self.n
 26 
 27 
 28 x = GenNum()
 29 y = GenNum()
 30 assert id(x) == id(y), "Not same object"
 31 print([next(x) for i in range(5)])
 32 print([next(y) for i in range(5)])
~                                        
```
