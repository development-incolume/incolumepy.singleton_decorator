#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Principal Module."""
from functools import lru_cache, wraps
from pathlib import Path

import toml

configproject = Path(__file__).parents[2].joinpath("pyproject.toml")
version_file = Path(__file__).parent.joinpath("version.txt")
try:
    version_file.write_text(f"{toml.load(configproject)['tool']['poetry']['version']}\n")
except FileNotFoundError:
    pass

__version__ = version_file.read_text().strip()


@lru_cache()
def singleton(cls):
    """Decorate for classes and definitions."""
    instances = {}

    @wraps(cls)
    def wrapper(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]

    return wrapper
