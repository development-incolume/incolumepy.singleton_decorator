#!/usr/bin/env bash
# -*- coding: utf-8 -*-
import re
import unittest

import pytest

from incolumepy.singleton_decorator import __version__, configproject, version_file, singleton


@pytest.mark.fasttest
def test_config_exist():
    assert configproject.is_file(), f"Ops: {configproject}"


@pytest.mark.fasttest
def test_versionfile_exist():
    assert version_file.is_file(), f"Ops: {version_file}"


@pytest.mark.parametrize(
    ["entrance", "expected"],
    (
        (__version__, True),
        ("0.0.1", True),
        ("0.1.0", True),
        ("1.0.0", True),
        ("1.0.1", True),
        ("1.1.1", True),
        ("1.0.1-dev0", True),
        ("1.0.1-dev.0", True),
        ("1.0.1-dev.1", True),
        ("1.0.1-dev.2", True),
        ("1.0.1-alpha.0", True),
        ("1.0.1-alpha.266", True),
        ("1.0.1-dev.0", True),
        ("1.0.1-beta.0", True),
    ),
)
def test_version(entrance, expected):
    assert re.fullmatch(r"\d\.\d\.\d(-\w+\.\d)?", __version__, flags=re.I)


@singleton
class GenNum:
    def __init__(self):
        self.n = -1

    def __iter__(self):
        return self

    def __next__(self):
        self.n += 1
        return self.n


@singleton
def gennum():
    n = 0
    while True:
        yield n
        n += 1


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.a = gennum()
        self.b = gennum()
        self.x = GenNum()
        self.y = GenNum()
        self.lista = list(range(10))

    def test_method_singleton_id(self):
        @singleton
        def tst():
            return

        self.assertEqual(id(tst()), id(tst()))

    def test_method_singleton(self):
        assert id(self.a) == id(self.b), "Not same object"

    def test_method_singleton_value(self):
        self.assertEqual([next(self.a) for x in range(5)], self.lista[:5])
        self.assertEqual([next(self.b) for x in range(5)], self.lista[5:])

    def test_class_singleton(self):
        self.assertEqual(id(self.x), id(self.y))

    def test_class_singleton_value(self):
        self.assertEqual([next(self.x) for x in range(5)], self.lista[:5])
        self.assertEqual([next(self.y) for x in range(5)], self.lista[5:])


if __name__ == "__main__":
    unittest.main()
